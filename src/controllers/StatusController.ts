import { Get } from "routing-controllers";

import { ServiceJsonController } from "../utils/serviceHelpers";

@ServiceJsonController("/")
export class StatusController {
    @Get("/status")
    getStatus(): string {
        return "Alive";
    }
}
