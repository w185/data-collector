import { Config, ConfigValue } from "typed-configs";

/**
 * I've created the config lib called 'typed-configs' to both validate config values (coming from a yaml file or env.args),
 * documentation, typings and most of all ease of use.
 */
@Config({ configYmlPath: `configs/${process.env.ENVIRONMENT}.yml` })
export class ServiceConfig {
    @ConfigValue({
        name: "ENVIRONMENT",
        description: "Server environment. Default is 'production'.",
        required: false
    })
    environment: string = "production";

    @ConfigValue({
        name: "PORT",
        description: "Server port",
        required: true
    })
    port!: number;
}
