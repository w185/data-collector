import { ExpressErrorMiddlewareInterface, HttpError } from "routing-controllers";
import { Request, Response } from "express";

import { ServiceError } from "../utils/ErrorCodes";
import { logger } from "../utils/logger";
import { ServiceMiddleware } from "../utils/serviceHelpers";

/**
 * Use a custom error handler (make sure to disable the default error handler) to prevent leaking sensitive information.
 */
@ServiceMiddleware({ type: "after" })
export class ErrorHandlerMiddleware implements ExpressErrorMiddlewareInterface {
    error(error: any, _request: Request, response: Response, next: (err?: any) => any): void {
        // 'error.errors' means validation errors. Instance checking didn't work on the class ValidationError.
        if (error.errors) {
            const validationErrors: object[] = error.errors.map((error: any) => error.constraints);
            error = ServiceError.badRequest(`Input validation failed: ${JSON.stringify(validationErrors)}`);
        } else if (error instanceof HttpError) {
            error = new ServiceError(error.httpCode, error.name, error.message);
        }

        if (error instanceof ServiceError) {
            response.status(error.status);
            response.json(error.toJSON());
        } else {
            logger.error("Internal Server Error", error);

            response.status(500);
            response.json(ServiceError.internalServiceError().toJSON());
        }

        next();
    }
}
