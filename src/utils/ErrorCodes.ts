export class ServiceError {
    constructor(readonly status: number, readonly name: string, readonly message?: string) {}

    static badRequest(message?: string) {
        return new ServiceError(401, "Bad Request", message);
    }

    static notFound(message?: string) {
        return new ServiceError(404, "Not Found", message);
    }

    static internalServiceError() {
        return new ServiceError(500, "Internal Server Error", "If this issue keeps occuring, please contact the IT support.");
    }

    toJSON() {
        return {
            status: this.status,
            name: this.name,
            message: this.message
        };
    }
}
