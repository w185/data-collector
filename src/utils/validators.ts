import moment from "moment";
import { ValidatorConstraint, ValidatorConstraintInterface } from "class-validator";

@ValidatorConstraint()
export class ShortDateValidator implements ValidatorConstraintInterface {
    validate(shortDate: string) {
        return moment(shortDate, "YYYY-MM-DD", true).isValid();
    }
}
