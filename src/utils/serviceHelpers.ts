import { Controller, JsonController, Middleware } from "routing-controllers";
import { Service } from "typedi";

// TypeDI@0.9.0+ won't create instances for unknown classes so we're combining both decorators here.
// https://github.com/typestack/routing-controllers/issues/642

export function ServiceController(...args: Parameters<typeof Controller>) {
    return <TFunction extends Function>(target: TFunction) => {
        Service()(target);
        Controller(...args)(target);
    };
}

export function ServiceJsonController(...args: Parameters<typeof Controller>) {
    return <TFunction extends Function>(target: TFunction) => {
        Service()(target);
        JsonController(...args)(target);
    };
}

export function ServiceMiddleware(...args: Parameters<typeof Middleware>) {
    return <TFunction extends Function>(target: TFunction) => {
        Service()(target);
        Middleware(...args)(target);
    };
}
