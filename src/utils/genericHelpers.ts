import { isInt } from "class-validator";
import { Configs } from "typed-configs";
import { ServiceConfig } from "../configs/ServiceConfig";

export const serviceConfig = Configs.get(ServiceConfig);

export function isReallyNaN(subject: number) {
    return typeof subject === "number" && isNaN(subject);
}

export function isValidIntIfSet(subject: number) {
    if (subject === undefined) return true;

    return !isReallyNaN(subject) && isInt(subject) && subject > 0;
}
