import "reflect-metadata";
import path from "path";
import express from "express";
import { Container } from "typedi";
import { useContainer, useExpressServer } from "routing-controllers";

import { serviceConfig } from "./utils/genericHelpers";
import { logger } from "./utils/logger";

// Routing Controllers supports DI and we use TypeDI for that
useContainer(Container);

const app = express();

// Nobody needs to know what we're using and which version it is :-)
app.disable("etag");
app.disable("x-powered-by");

useExpressServer(app, {
    // Support *.ts files for ts-node-dev.
    // When bundling with webpack this won't work and you need to require all files manually or by using require.context.
    controllers: [path.join(__dirname, "/controllers/**/*{.js,.ts}")],
    middlewares: [path.join(__dirname, "/middlewares/*{.js,.ts}")],
    defaultErrorHandler: false,
    validation: true
});

app.listen(serviceConfig.port, () => {
    logger.info(`API listening on port ${serviceConfig.port}`);
});
