FROM node:14-alpine

RUN apk add --update --no-cache bash

COPY . /usr/src/data-collector

WORKDIR /usr/src/data-collector

RUN npm install && npm run build

# EXPOSE 1337

# ENV NODE_ENV=production
# CMD ["node", "dist/index.js"]
